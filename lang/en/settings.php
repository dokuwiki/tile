<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * English language file
 */
 
// for the configuration tile plugin
$lang['iconSize']		= 'tile icon size';
$lang['sampleDelai']	= 'cache time delay for sample (sec)';
?>
