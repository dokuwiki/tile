<?php
/**
 * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
 * @author     Francois Merciol <dokuplugin@merciol.fr>
 *
 * French language file
 */

// for the configuration tile plugin
$lang['iconSize']		= 'taille de l\'icon de tuile';
$lang['sampleDelai']	= 'temps de cache pour l\'exemple (en secondes)';
?>
