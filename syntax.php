<?php
  /**
   * @license    http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html
   * @author     Francois <dokuplugin@merciol.fr>
   *
   * Plugin touchtile: display URL in tiles
   */
 
if (!defined ('DOKU_INC'))
    define ('DOKU_INC', realpath (dirname (__FILE__).'/../../').'/');
if (!defined ('DOKU_PLUGIN'))
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');

// ============================================================
class syntax_plugin_tile extends DokuWiki_Syntax_Plugin {
 
  // ============================================================
  function getType () { return 'substition'; }
  function getPType () { return 'block';}
  function getSort () { return 299; }
  function connectTo ($mode) {
    $this->Lexer->addEntryPattern ('<tile[^>]*>', $mode, 'plugin_tile');
  }
  function postConnect() {
    $this->Lexer->addExitPattern ('</tile>', 'plugin_tile');
  }
 
  // ============================================================
  function handle ($match, $state, $pos, Doku_Handler $handler){
    switch ($state) {
    case DOKU_LEXER_ENTER:
      return array ($state, trim (substr ($match, 5, -1))); // "<title" => 5 ">" => 1
    case DOKU_LEXER_UNMATCHED:
      return array ($state, $match);
    case DOKU_LEXER_EXIT:
      return array ($state);
    }
    return false;
  }

  var $iconSize;
  var $imgAttr;

  // ============================================================
  function render ($mode, Doku_Renderer $renderer, $indata) {
    if ($mode != 'xhtml')
      return false;
    if (empty ($indata))
      return false;
    list ($instr, $data) = $indata;
    switch ($instr) {
    case DOKU_LEXER_ENTER :
      if (preg_match_all ("#(\"[^\"]*\")*help(\"[^\"]*\")*#", $data, $dumy) > 0)
	$this->help ($renderer);
      $this->iconSize = $this->getConf ('iconSize');
      if (preg_match_all ("#(\"[^\"]*\")*width=\"(?<width>[^\"]*)\"(\"[^\"]*\")*#", $data, $dumy) > 0) 
	$this->iconSize = $dumy ['width'][0];
      $this->imgAttr = ' width="'.$this->iconSize.'"';
      $renderer->doc .= '      <div class="tiles">';
      break;
    case DOKU_LEXER_EXIT :
      $renderer->doc .= '      </div>';
      break;
    case DOKU_LEXER_UNMATCHED :
      $data = trim ($data);
      if (empty ($data))
	return false;
      global $_REQUEST;
      foreach (explode ("\n", $data) as $line) {
	$line = trim ($line);
	if (!$line)
	  continue;
	$line = preg_replace ("#\s+\|#", "|", $line);
	$line = preg_replace ("#\|\s+#", "|", $line);
	$line = trim ($line,'|');
	list ($id, $title, $img, $email, $name) = explode ("|", $line);
	if (!$id)
	  continue;
	$email = obfuscate ($email);
	$mailto = $email ? '<a class="mail JSnocheck" href="mailto:'.$email.'" >'.($name ? $name : $email).'</a>' : "";
	$renderer->doc .= '
       <div class="tile">
        <a href="'.(str_starts_with ($id, 'http') ? $id : wl ($id)).'">
         <p>'.$title.'</p>
         <div class="img"><span></span><img src="'.ml ($img, array ('cache'=>$_REQUEST['cache'], 'w'=>$this->iconSize)).'"'.$this->imgAttr.' alt="'.$title.'"/></div>
        </a>
        <p>'.$mailto.'</p>
       </div>';
      }
      break;
    }
    return true;
  }

    // ============================================================
  function help (Doku_Renderer $renderer) {
    $url = "http://admin.parlenet.org/plugins/tile/";
    $renderer->doc .=
      '      <h1>Tile Help</h1>'.NL.
      '      <ul>'.NL.
      '        <li>Syntax : <b>&lt;tile [help] [width=pp]&gt;</b><br/>'.
      '           | :dokuwiki:namespace:page | Short Description | :dokuwiki:namespace:icon.png | member.mel@some.where.org | nickname |<br/>'.
      '           | http... | Short Description | :dokuwiki:namespace:icon.png | member.mel@some.where.org | nickname |<br/>'.
      '           ...<br/>'.
      '           <b>&lt;/tile&gt;</b></li>'.NL.
      '        <li>Info : <a class="urlextern" rel="nofollow" title="'.$url.'" href="'.$url.'">'.$url.'</a></li>'.NL.
      '      </ul>'.NL;
  }

  // ============================================================
} // syntax_plugin_tile
?>
